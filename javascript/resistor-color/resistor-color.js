//
// This is only a SKELETON file for the 'Resistor Color' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export const colorCode = s => COLORS.indexOf(s);

export const COLORS = [
  "black",
  "brown",
  "red",
  "orange",
  "yellow",
  "green",
  "blue",
  "violet",
  "grey",
  "white",
];

/*console.log(colorCode('brown'));
console.log(colorCode('white'));
console.log(colorCode('orange'));
console.log(COLORS);*/


