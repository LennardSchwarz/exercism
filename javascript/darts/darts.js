export const score = (x, y) => {
  function calculateDistanceToCenter(x, y) {
    return Math.sqrt(x ** 2 + y ** 2);
  }

  const distanceToCenter = calculateDistanceToCenter(x, y);

  let score;

  //score logic
  switch (true) {
    case distanceToCenter <= 1:
      score = 10;
      break;
    case distanceToCenter <= 5:
      score = 5;
      break;
    case distanceToCenter <= 10:
      score = 1;
      break;
    default:
      score = 0;
  }

  return score;
};
