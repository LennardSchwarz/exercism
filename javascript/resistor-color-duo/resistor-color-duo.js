//
// This is only a SKELETON file for the 'Resistor Color Duo' exercise. It's been provided as a
// convenience to get you started writing code faster.
//
const COLORS = [
  "black",
  "brown",
  "red",
  "orange",
  "yellow",
  "green",
  "blue",
  "violet",
  "grey",
  "white",
];

export const decodedValue = (array) => {
  let indexArray = [];
  array.forEach(e => {
    const index = COLORS.indexOf(e);
    indexArray.push(index);
  });

  return `${indexArray[0]}${indexArray[indexArray.length-1]}`
};


//console.log(decodedValue(['brown', 'black']));




