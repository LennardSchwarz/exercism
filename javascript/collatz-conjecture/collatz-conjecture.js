//
// This is only a SKELETON file for the 'Collatz Conjecture' exercise. It's been provided as a
// convenience to get you started writing code faster.
//

export const steps = (n = 1) => {
  if (n <= 0) {
    throw 'Only positive numbers are allowed'
  }

  let counter = 0;
  while (n !== 1) {
    counter ++;

    if (n%2 === 0) {
      n = n/2;
    } else {
      n = n*3 + 1;
    }
  }
  return counter
};
